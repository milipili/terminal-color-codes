# Terminal Colours (from xrdb)

## XTerm Screenshot

![XTerm screenshot](screenshot.png)


## .Xresources

```
*background: #232C31
*foreground: #C5C8C6
! black
*color0: #3F3F3F
*color8: #425059
! red
*color1: #F12626
*color9: #D06060
! green
*color2: #56B384
*color10: #6BDBA3
! yellow
*color3: #F58E4A
*color11: #F0DCA5
! blue
*color4: #83A6C9
*color12: #94BFF3
! magenta
*color5: #CF66AE
*color13: #F77ED5
! cyan
*color6: #77BBBE
*color14: #85DFE2
! white
*color7: #DCDCDC
*color15: #FFFFFF
```

To load the X resources:

```
$ xrdb -load ~/.Xresources 
```

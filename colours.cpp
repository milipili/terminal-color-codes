#include <array>
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <sstream>
#include <stdlib.h>

namespace {

enum class Color {
	black,
	red,
	green,
	yellow,
	blue,
	magenta,
	cyan,
	white
};

constexpr Color operator ++ (Color& c) { return c = (Color)(std::underlying_type<Color>::type(c) + 1); }
constexpr Color operator * (Color c) { return c; }

constexpr Color begin(Color)  { return Color::black; }
constexpr Color end(Color)    { Color l = Color::white; return ++l; }

constexpr uint32_t colorCount() {
	return 1 + std::underlying_type<Color>::type(Color::white) - std::underlying_type<Color>::type(Color::black);
}

constexpr const char invalidHexaColor[] = "#??????";

struct ColorPalette {
	std::array<std::string, colorCount()> light;
	std::array<std::string, colorCount()> dark;
};

std::ostream& operator << (std::ostream& o, Color color) {
	switch (color) {
		case Color::black:   o << "BLK"; break;
		case Color::red:     o << "RED"; break;
		case Color::green:   o << "GRN"; break;
		case Color::yellow:  o << "YEL"; break;
		case Color::blue:    o << "BLU"; break;
		case Color::magenta: o << "MAG"; break;
		case Color::cyan:    o << "CYN"; break;
		case Color::white:   o << "WHT"; break;
	}
	return o;
}

std::string exec(const char* cmd) {
	std::array<char, 128> buffer;
	std::string result;
	std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
	if (!pipe)
		throw std::runtime_error("popen() failed!");
	while (!feof(pipe.get())) {
		if (fgets(buffer.data(), 128, pipe.get()) != NULL)
			result += buffer.data();
	}
	return result;
}

void readColorTable(ColorPalette& palette) {
	for (auto& item: palette.light)
		item = invalidHexaColor;
	for (auto& item: palette.dark)
		item = invalidHexaColor;
	// output example:
	// *background:	#232C31
	// *foreground:	#C5C8C6
	// *color0:	#2D3C46
	// *color8:	#425059
	std::istringstream xrgb(exec("xrdb -query"));
	std::string line;
	while (std::getline(xrgb, line)) {
		if (line.compare(0, 6, "*color") != 0)
			continue;
		auto index = strtoul(line.c_str() + 6, nullptr, 10);
		if (errno != 0 or index >= colorCount() * 2)
			continue;
		auto hexaStart = line.find_last_of('#');
		if (index < colorCount())
			palette.light[index].assign(line, hexaStart, std::string::npos);
		else
			palette.dark[index - colorCount()].assign(line, hexaStart, std::string::npos);
	}
}

void printHeader() {
	for (auto color: Color())
		std::cout << "     " << color << "   ";
	std::cout << '\n';
}

void printLineSeparator() {
	for (uint32_t i = 0; i != colorCount(); ++i) {
		for (uint32_t x = 0; x != 11; ++x)
			std::cout << "\u2500"; // -
	}
	std::cout << '\n';
}

template<class T> void printColors(const T& array, char range) {
	constexpr const char block[] = "\u2588";
	for (auto color: Color()) {
		auto  index = std::underlying_type<Color>::type(color);
		auto& hexa  = array[index];
		std::cout << " \e[" << range << index << "m" << block << ' ' << hexa << "\e[0m ";
	}
	std::cout << '\n';
}

} // namespace


int main() {
	ColorPalette palette;
	readColorTable(palette);
	printHeader();
	printLineSeparator();
	printColors(palette.light, '3');
	printColors(palette.dark,  '9');
	return 0;
}
